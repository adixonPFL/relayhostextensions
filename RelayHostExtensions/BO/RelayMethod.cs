﻿namespace RelayHostExtensions.BO
{
    using System.Reflection;

    internal class RelayMethod
    {
        public MethodInfo MethodInfo { get; set; }
        public string HttpMethod { get; set; }
    }
}
