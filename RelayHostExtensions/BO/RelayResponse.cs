﻿namespace RelayHostExtensions.BO
{
    using System.IO;
    using System.Net;

    public class RelayResponse
    {
        /// <summary>
        /// Gets or sets the Http status code to be returned
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Gets or sets a text description of the HTTP status code to be returned
        /// </summary>
        public string StatusDescription { get; set; }

        /// <summary>
        /// A stream object to which a response can be written
        /// </summary>
        public Stream Stream { get; set; }
    }
}
