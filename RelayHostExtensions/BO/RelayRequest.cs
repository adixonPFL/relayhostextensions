﻿namespace RelayHostExtensions.BO
{
    using System;
    using System.IO;
    using System.Net;

    public class RelayRequest
    {
        /// <summary>
        /// A System.Boolean value that indicates whether the request has associated body data.
        /// </summary>
        public bool HasEntityBody { get; set; }

        /// <summary>
        /// The collection of header name/value pairs received in the request.
        /// </summary>
        public WebHeaderCollection Headers { get; set;  }
        
        /// <summary>
        /// the HTTP method specified by the client.
        /// </summary>
        public string HttpMethod { get; set;  }

        /// <summary>
        /// a stream that contains the body data sent by the client.
        /// </summary>
        public Stream InputStream { get; set;  }

        /// <summary>
        /// the client IP address and port number from which the request originated.
        /// </summary>
        public IPEndPoint RemoteEndPoint { get; set; }

        /// <summary>
        /// the Uri requested by the client.
        /// </summary>
        public Uri Url { get; set; }
    }
}
