﻿namespace RelayHostExtensions.BLL.Extensions
{
    using Microsoft.Azure.Relay;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using RelayHostExtensions.BLL.Abstracts;
    using RelayHostExtensions.BLL.Attributes;
    using RelayHostExtensions.BLL.Binders;
    using RelayHostExtensions.BO;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;

    public static class HostExtensions
    {
        /// <summary>
        /// Established listeners
        /// </summary>
        private static readonly Dictionary<Uri, HybridConnectionListener> _listeners = new Dictionary<Uri, HybridConnectionListener>();

        /// <summary>
        /// A mapping of the discovered target methods
        /// </summary>
        private static Dictionary<string, RelayMethod> _relayEndpoints = null;

        /// <summary>
        /// Close a single listener
        /// </summary>
        /// <param name="uri">The listener uri</param>
        public async static Task CloseListener(Uri uri)
        {
            if (_listeners.Remove(uri, out HybridConnectionListener listener)) {
                await CloseListener(listener).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Closes all listeners
        /// </summary>
        public async static Task CloseListeners()
        {
            while (_listeners.Any())
            {
                if (_listeners.Remove(_listeners.First().Key, out HybridConnectionListener listener)) 
                {
                    await CloseListener(listener).ConfigureAwait(false);
                }
            }
        }

        /// <summary>
        /// Configures a Service Bus Relay listener targeting classes that extend RelayController
        /// </summary>
        /// <param name="host">The host</param>
        /// <param name="baseUri">The base uri of the relay endpoint</param>
        /// <param name="tokenProvider">The token provider</param>
        /// <param name="logger">The logger</param>
        /// <returns>The host</returns>
        public static IHost ConfigureRelayController(this IHost host, Uri baseUri, TokenProvider tokenProvider, ILogger logger = null)
        {
            ValidateParameters(baseUri, tokenProvider);

            if (logger == null)
            {
                logger = (host.Services.GetService(typeof(ILoggerFactory)) as ILoggerFactory).CreateLogger("HostExtensions");
            }

            // Create the listener
            HybridConnectionListener listener = new HybridConnectionListener(baseUri, tokenProvider);

            // Subscribe to the status events.
            listener.Connecting += (o, e) => { LogInformation(logger, "Connecting to Relay on [{BaseUri}]", baseUri); };
            listener.Offline += (o, e) => { LogInformation(logger, "Relay Offline on [{BaseUri}]", baseUri); };
            listener.Online += (o, e) => { LogInformation(logger, "Relay Online on [{BaseUri}]", baseUri); };

            if (_relayEndpoints == null) 
            {
                List<KeyValuePair<string, RelayMethod>> relayEndpoints = new List<KeyValuePair<string, RelayMethod>>();

                foreach(Type type in GetRelayControllers())
                {
                    relayEndpoints.AddRange(GetRelayMethods(type, logger));
                }

                _relayEndpoints = relayEndpoints.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }

            EstablishRequestHandler(host, listener, logger).Wait();

            return host;
        }

        /// <summary>
        /// Validates the parameters are correct
        /// </summary>
        /// <param name="baseUri">The base uri</param>
        /// <param name="tokenProvider">The token provider</param>
        private static void ValidateParameters(Uri baseUri, TokenProvider tokenProvider)
        {
            if (baseUri == null)
            {
                throw new ArgumentNullException(nameof(baseUri));
            }

            if (_listeners.ContainsKey(baseUri))
            {
                throw new ArgumentException("Only one configuration is allowed for each baseUri");
            }

            if (!$"{baseUri}".StartsWith("sb"))
            {
                throw new ArgumentException("Uri must be a service bus relay endpoint");
            }

            if (tokenProvider == null)
            {
                throw new ArgumentNullException(nameof(tokenProvider));
            }
        }

        /// <summary>
        /// Close a listener
        /// </summary>
        /// <param name="listener">The listener to close</param>
        private async static Task CloseListener(HybridConnectionListener listener)
        {
            try
            {
                await listener.CloseAsync().ConfigureAwait(false);
            }
            catch (Exception)
            {
                // Do not block execution. Listener is likely already closed.
            }

        }

        /// <summary>
        /// Establishes a request handler for a given listener
        /// </summary>
        /// <param name="host">The host</param>
        /// <param name="listener">The listener</param>
        /// <param name="logger">The logger</param>
        private async static Task EstablishRequestHandler(IHost host, HybridConnectionListener listener, ILogger logger = null)
        {
            // Provide an HTTP request handler
            listener.RequestHandler = async (context) =>
            {
                LogInformation(logger, "Request received for [{RequestUrl}]", context.Request.Url);

                string relativePath = listener.Address.MakeRelativeUri(context.Request.Url).ToString().ToLower();

                string[] pathAndQuery = relativePath.Split('?', 2);

                if (_relayEndpoints.TryGetValue($"{context.Request.HttpMethod.ToLower()}|{pathAndQuery[0]}", out RelayMethod method))
                {
                    Type controllerType = method.MethodInfo.DeclaringType;
                    object controller = host.Services.GetRequiredService(controllerType);

                    IList<object> parameters = new List<object>();

                    if (pathAndQuery.Length > 1)
                    {
                        string[] queryParameters = pathAndQuery[1].Split('&');

                        foreach (string queryParameter in queryParameters)
                        {
                            string[] keyAndValue = queryParameter.Split('=');

                            if (keyAndValue[1].Length == 0)
                            {
                                parameters.Add(null);
                            }
                            else
                            {
                                parameters.Add(keyAndValue[1]);
                            }
                        }
                    }

                    MethodInfo setContextMethod = controller.GetType().GetMethod("SetContext");
                    setContextMethod.Invoke(controller,
                        new[] { 
                            new RelayRequest {
                                HasEntityBody = context.Request.HasEntityBody,
                                Headers = context.Request.Headers,
                                HttpMethod = context.Request.HttpMethod,
                                InputStream = context.Request.InputStream,
                                RemoteEndPoint = context.Request.RemoteEndPoint,
                                Url = context.Request.Url
                            }
                    });

                    RelayResponse relayResponse = null;

                    if (method.MethodInfo.ReturnType == typeof(Task<RelayResponse>))
                    {
                        Task<RelayResponse> relayResponseTask = method.MethodInfo.Invoke(controller, BindingFlags.InvokeMethod, new RelayControllerBinder(), parameters.ToArray(), CultureInfo.CurrentCulture) as Task<RelayResponse>;
                        relayResponse = await relayResponseTask.ConfigureAwait(false);
                    }
                    else
                    {
                        relayResponse = method.MethodInfo.Invoke(controller, BindingFlags.InvokeMethod, new RelayControllerBinder(), parameters.ToArray(), CultureInfo.CurrentCulture) as RelayResponse;
                    }

                    context.Response.StatusCode = relayResponse?.StatusCode ?? HttpStatusCode.OK;
                    context.Response.StatusDescription = relayResponse?.StatusDescription;

                    if (relayResponse?.Stream != null)
                    {
                        System.IO.Stream stream = context.Response.OutputStream;
                        await stream.CopyToAsync(relayResponse.Stream).ConfigureAwait(false);
                    }

                    context.Response.Close();
                }
                else
                {
                    context.Response.StatusCode = HttpStatusCode.BadRequest;
                    context.Response.Close();
                }
            };

            await listener.OpenAsync().ConfigureAwait(false);
            LogInformation(logger, "Server listening on [{BaseUri}]", listener.Address);

            _listeners.Add(listener.Address, listener);
        }

        /// <summary>
        /// Get all classes whose types are assignable from RelayController
        /// </summary>
        /// <returns>The classes that extend RelayController</returns>
        private static IEnumerable<Type> GetRelayControllers()
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).Where(a => typeof(RelayController).IsAssignableFrom(a) && !a.IsInterface && !a.IsAbstract);
        }

        /// <summary>
        /// Gets relay methods for a given controller
        /// </summary>
        /// <param name="relayController">The relay controller</param>
        /// <param name="logger">The logger</param>
        /// <returns>Methods that have a [RelayRoute] attribute</returns>
        private static IEnumerable<KeyValuePair<string, RelayMethod>> GetRelayMethods(Type relayController, ILogger logger = null) 
        {
            string classPrefix = string.Empty;

            if (relayController.GetCustomAttributes().FirstOrDefault(a => a.GetType() == typeof(RelayPrefixAttribute)) is RelayPrefixAttribute prefixAttribute)
            {
                classPrefix = prefixAttribute.GetPrefix().Trim('/');
            }
            else
            {
                classPrefix = relayController.Name.EndsWith("Controller") ? relayController.Name.Substring(0, relayController.Name.Length - "Controller".Length) : relayController.Name;
            }

            foreach (MethodInfo methodInfo in relayController.GetMethods())
            {
                if (methodInfo.ReturnType != typeof(RelayResponse) && methodInfo.ReturnType != typeof(Task<RelayResponse>))
                {
                    continue;
                }

                StringBuilder endpoint = new StringBuilder(classPrefix);
                endpoint.Append('/');

                if (methodInfo.GetCustomAttributes().FirstOrDefault(a => a.GetType() == typeof(RelayRouteAttribute)) is RelayRouteAttribute routeAttribute)
                {
                    string route = routeAttribute.GetRoute().Trim('/');

                    endpoint.Append(string.IsNullOrWhiteSpace(route) ? methodInfo.Name : route);
                }
                else
                {
                    continue;
                }

                RelayMethod relayMethod = new RelayMethod
                {
                    MethodInfo = methodInfo
                };

                if (methodInfo.GetCustomAttributes().FirstOrDefault(a => a.GetType() == typeof(RelayMethodAttribute)) is RelayMethodAttribute methodAttribute)
                {
                    relayMethod.HttpMethod = methodAttribute.GetMethod()?.ToLower();
                }
                else
                {
                    continue;
                }

                LogInformation(logger, "Found relay endpoint: [{HttpMethod}] - [{Endpoint}]", relayMethod.HttpMethod, endpoint);
                yield return new KeyValuePair<string, RelayMethod>($"{relayMethod.HttpMethod}|{endpoint.ToString().ToLower()}", relayMethod);
            }
        }
        
        /// <summary>
        /// Logs information if a logger is not null
        /// </summary>
        /// <param name="logger">The logger</param>
        /// <param name="text">The text to log</param>
        /// <param name="args">The arguments</param>
        private static void LogInformation(ILogger logger, string text, params object[] args)
        {
            if (logger != null)
            {
                logger.LogInformation(text, args);
            }
        }
    }
}
