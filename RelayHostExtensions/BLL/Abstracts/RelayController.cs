﻿namespace RelayHostExtensions.BLL.Abstracts
{
    using RelayHostExtensions.BO;

    /// <summary>
    /// Abstract class extended by classes meant to serve as relay controllers
    /// </summary>
    public abstract class RelayController
    {
        /// <summary>
        /// The context provided by the listener
        /// </summary>
        protected RelayRequest Context { get; private set; }

        /// <summary>
        /// Sets the context
        /// </summary>
        /// <param name="context">The context</param>
        public void SetContext(RelayRequest context)
        {
            Context = context;
        }
    }
}
