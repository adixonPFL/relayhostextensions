﻿namespace RelayHostExtensions.BLL.Attributes
{
    using System;

    /// <summary>
    /// Defines a route prefix for all methods in the class
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class RelayPrefixAttribute : Attribute
    {
        /// <summary>
        /// The prefix
        /// </summary>
        private readonly string _prefix;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="prefix">The prefix</param>
        public RelayPrefixAttribute(string prefix)
        {
            _prefix = prefix ?? string.Empty;
        }

        /// <summary>
        /// Gets the prefix for the class
        /// </summary>
        /// <returns>The prefix for the class</returns>
        public string GetPrefix()
        {
            return _prefix;
        }
    }
}
