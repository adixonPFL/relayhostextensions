﻿namespace RelayHostExtensions.BLL.Attributes
{
    using System;

    /// <summary>
    /// Defines the HttpMethod for the method
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class RelayMethodAttribute : Attribute
    {
        /// <summary>
        /// The httpMethod
        /// </summary>
        private readonly string _method;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="method">The http method</param>
        public RelayMethodAttribute(string method)
        {
            _method = method;
        }

        /// <summary>
        /// Gets the http method
        /// </summary>
        /// <returns>The http method</returns>
        public string GetMethod()
        {
            return _method;
        }
    }
}
