﻿namespace RelayHostExtensions.BLL.Attributes
{
    using System;

    /// <summary>
    /// Signifies to configuration that this method is intended as an available Relay route
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class RelayRouteAttribute : Attribute
    {
        /// <summary>
        /// The route
        /// </summary>
        private readonly string _route;

        /// <summary>
        /// Emtpy constructor
        /// </summary>
        /// <remarks>
        /// Uses the name of the method for the route
        /// </remarks>
        public RelayRouteAttribute()
        {
            _route = string.Empty;
        }

        /// <summary>
        /// Route constructor
        /// </summary>
        /// <param name="route">The route hname</param>
        public RelayRouteAttribute(string route)
        {
            _route = route ?? string.Empty;
        }

        /// <summary>
        /// Gets the route
        /// </summary>
        /// <returns>The route</returns>
        public string GetRoute()
        {
            return _route;
        }
    }
}
